using Xunit;

namespace BusinessLibrary.Test
{
  public class TestProductHelper
  {
    [Theory]
    [InlineData(10.0, 12.0)]
    [InlineData(20.0, 24.0)]
    public void TestGetValueAddedPrice(decimal rawPrice, decimal expectedVatPrice)
    {

      decimal actualVatPrice = ProductHelper.GetValueAddedPrice(rawPrice);
      decimal epsilon = 0.000001M;
      Assert.InRange(actualVatPrice, expectedVatPrice - epsilon, expectedVatPrice + epsilon);
    }
  }
}