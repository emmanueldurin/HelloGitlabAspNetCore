﻿using System.Diagnostics;

namespace BusinessLibrary
{
  public class ProductHelper
  {
    public static decimal GetValueAddedPrice( decimal rawPrice)
    {
      return rawPrice * 1.2M;
    }
  }
}